package facci.arcentalesjordan.registro;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;

public class BDHelper extends SQLiteOpenHelper {

String tabla ="CREATE TABLE REGISTRO(NOMBRE TEXT PRIMARY KEY, TRABAJO TEXT, HORADETABAJO TEXT, PRECIO TEXT, VALORTOTAL TEXT)";

    public BDHelper (Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context,name,factory,version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tabla);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public String LeerTodos() {
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {"nombre", "trabajo", "hora", "precio", "valor"};
        String sqlTable = "Universidades";
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                result += "nombre " + c.getInt(c.getColumnIndex("nombre")) + "\n" +
                        "trabajo" + c.getString(c.getColumnIndex("trabajo")) + "\n" +
                        "hora " + c.getString(c.getColumnIndex("hora")) + "\n" +
                        "precio " + c.getString(c.getColumnIndex("precio")) + "\n" +
                        "valor " + c.getString(c.getColumnIndex("valor")) + "\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }
}
package facci.arcentalesjordan.registro;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Lista extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        cargar();
    }
    public void cargar() {

        BDHelper basehelper = new BDHelper(this, "DEMODB", null, 1);
        SQLiteDatabase db = basehelper.getReadableDatabase();
        if (db != null) {

            Cursor c = db.rawQuery("select *from REGISTRO", null);
            int cantidad = c.getCount();
            int i = 0;
            String[] Arreglo = new String[cantidad];
            if (c.moveToFirst()) {
                do {
                    String linea = c.getInt(0) + ""
                            + c.getString(1) + c.getString(2)
                            + c.getInt(3) + c.getInt(4) + c.getInt(5);
                    Arreglo[i] = linea;
                    i++;
                } while (c.moveToNext());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Arreglo);
            ListView Lista = (ListView) findViewById(R.id.Lista);
            Lista.setAdapter(adapter);
        }
    }
}


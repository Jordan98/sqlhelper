package facci.arcentalesjordan.registro;

import androidx.appcompat.app.AppCompatActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {


    public EditText editTextnombre, editTexttrabajo, editTextprecio, editTexthora;
    public TextView textViewvalor;
    private Button buttoncalcular, buttonguardar, buttonmostrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editTextnombre = (EditText) findViewById(R.id.editTextNOMBRE);
        editTexttrabajo = (EditText) findViewById(R.id.editTextTRABAJO);
        editTexthora = (EditText) findViewById(R.id.editTextHORAS);
        editTextprecio = (EditText) findViewById(R.id.editTextPRECIO);
        textViewvalor = (TextView) findViewById(R.id.textViewvalor);
        buttoncalcular = (Button) findViewById(R.id.buttonCALCULAR);
        buttonguardar = (Button) findViewById(R.id.buttonGUARDAR);






        buttoncalcular.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View argo) {
                float precio, salario;
                int nuHoras;
                precio = Float.parseFloat(editTextprecio.getText().toString());
                nuHoras = Integer.parseInt(editTexthora.getText().toString());
                salario = precio * nuHoras;
                textViewvalor.setText(salario+" ");
            }
        });
    }

    public void GuardarDatos(View view){
        String nombre = editTextnombre.getText().toString();
        String trabajo = editTexttrabajo.getText().toString();
        String hora = editTexthora.getText().toString();
        String precio=  editTextprecio.getText().toString();
        String valor = textViewvalor.getText().toString();

        BDHelper basedeDatos = new BDHelper(this, "DEMODB", null,1);

        SQLiteDatabase db = basedeDatos.getWritableDatabase();
        if (db != null){
            ContentValues registronuevo = new ContentValues();
            registronuevo.put("nombre", nombre);
            registronuevo.put("trabajo", trabajo);
            registronuevo.put("hora", hora);
            registronuevo.put("precio", precio);
            registronuevo.put("valor", valor);

            db.insert("tarea",null,registronuevo);
            Toast.makeText(this, "datos almacenados", Toast.LENGTH_SHORT).show();

        }
    }




}